var express = require('express');
var router = express.Router();

var controller=require('../controllers/controller')

router.get('/',controller.index);
router.get('/taches',controller.getLesTaches);
router.get('/taches/:id',controller.getOneTache);
module.exports = router;
