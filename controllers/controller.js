/* GET home page. */
exports.index= function(req, res, next) {
    res.render('index', { title: 'todoList' });
};

/*GET Liste des tâches*/
exports.getLesTaches=function(req, res, next) {
    res.render('taches', { title: 'Liste des taches' });
};

/*GET uneTache */
exports.getOneTache=function(req, res, next) {
    res.send('NOT IMPLEMENTED: Detail Tache id : ' + req.params.id);
};